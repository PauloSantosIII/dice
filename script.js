let count = Array(11).fill(0)
function dadosRolando() {
    for (let jogada = 0; jogada <= 1000; jogada++) {
        d1 = Math.ceil(Math.random() * 6)
        d2 = Math.ceil(Math.random() * 6)
        rodada = d1 + d2
        count[rodada - 2]++
    }
    tela.appendChild(document.createElement('h1')).innerHTML = 'Rolando os Dados'

    tela.appendChild(document.createElement('h2')).innerHTML = 'Contagem'

    for (let i = 0; i < 11; i++) {
        tela.appendChild(document.createElement('p')).innerHTML = `${i + 2} : ${count[i]}`
    }
    tela.appendChild(document.createElement('h2')).innerHTML = 'Gráfico'
    
    for (let j = 0; j < 11; j++) {
        let barra = document.createElement('div')
        barra.style.height = `${count[j]}px`
        barra.innerHTML = `${count[j]}x`
        tela.appendChild(barra)
    }    
}
dadosRolando()